package com.tsarik.application.preferences;


/**
 * Interface for object witch can apply some preferences.
 * 
 * @author Phil Tsarik
 *
 */
public interface PreferencesApplicable {
	
	/**
	 * Apply preferences.
	 */
	public void applyPreferences();
	
}
