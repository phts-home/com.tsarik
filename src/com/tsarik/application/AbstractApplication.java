package com.tsarik.application;


/**
 * Abstract application class with main method.
 * 
 * @version 0.1
 * @author Phil Tsarik
 *
 */
public abstract class AbstractApplication {
	
	public abstract void exit();
	
}
