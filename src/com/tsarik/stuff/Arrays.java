package com.tsarik.stuff;

public class Arrays {
	
	public static void printArray(String name, int[] ar) {
		if (ar == null) {
			System.out.println(name + " == null");
			return;
		}
		System.out.print(name + " == [");
		if (ar.length == 0) {
			System.out.println("]");
			return;
		}
		for (int i = 0; i < ar.length-1; i++) {
			System.out.print(ar[i] + ", ");
		}
		System.out.println(ar[ar.length-1] + "]");
	}
	
	public static void printArray(String name, byte[] ar) {
		if (ar == null) {
			System.out.println(name + " == null");
			return;
		}
		System.out.print(name + " == [");
		if (ar.length == 0) {
			System.out.println("]");
			return;
		}
		for (int i = 0; i < ar.length-1; i++) {
			System.out.print(ar[i] + ", ");
		}
		System.out.println(ar[ar.length-1] + "]");
	}
	
	public static String arrayToString(int[] ar) {
		String s = new String("");
		s = s.concat("[");
		if ( (ar == null) || (ar.length == 0) ) {
			s = s.concat("]");
			return s;
		}
		for (int i = 0; i < ar.length-1; i++) {
			s = s.concat(ar[i] + ", ");
		}
		s = s.concat(ar[ar.length-1] + "]");
		return s;
	}
	
	public static String arrayToString(byte[] ar) {
		String s = new String("");
		s = s.concat("[");
		if ( (ar == null) || (ar.length == 0) ) {
			s = s.concat("]");
			return s;
		}
		for (int i = 0; i < ar.length-1; i++) {
			s = s.concat(ar[i] + ", ");
		}
		s = s.concat(ar[ar.length-1] + "]");
		return s;
	}
	
}
