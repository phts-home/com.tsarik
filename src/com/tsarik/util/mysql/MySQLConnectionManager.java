package com.tsarik.util.mysql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


/**
 * 
 * @author Phil Tsarik
 *
 */
public class MySQLConnectionManager {
	
	protected MySQLConnectionManager() throws ClassNotFoundException {
		Class.forName("com.mysql.jdbc.Driver");
	}
	
	public static synchronized MySQLConnectionManager getInstance(String dbHost, String dbName, String user, String password) throws SQLException, ClassNotFoundException {
		if (instance == null) {
			instance = new MySQLConnectionManager();
		}
		createConnection(dbHost, dbName, user, password);
		return instance;
	}
	
	public ResultSet anyQuery(String queryText) throws SQLException {
		rs = null;
		stmt = con.createStatement();
		if (stmt.execute(queryText) == true) {
			rs = stmt.getResultSet();
		}
		return rs;
	}
	
	public ResultSet selectQuery(String queryText) throws SQLException {
		stmt = con.createStatement();
		rs = stmt.executeQuery(queryText);
		return rs;
	}
	
	public void closeResources() throws SQLException {
		if (rs != null) {
			rs.close();
			rs = null;
		}
		if (stmt != null) {
			stmt.close();
			stmt = null;
		}
	}
	
	public int getResultSetRowCount() throws SQLException {
		int count = 0;
		if (rs != null) {
			int curpos = rs.getRow();
			rs.beforeFirst();
			while (rs.next()) {
				count++;
			}
			if (curpos != 0) {
				rs.absolute(curpos);
			}
		}
		return count;
	}
	
	protected static Connection con;
	protected ResultSet rs;
	
	protected static void createConnection(String dbHost, String dbName, String user, String password) throws SQLException {
		String url = "jdbc:mysql://"+dbHost+"/" + dbName;
		con = DriverManager.getConnection(url, user, password);
	}
	
	private static MySQLConnectionManager instance;
	private Statement stmt;
	
}
