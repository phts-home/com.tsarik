package com.tsarik.dialogs;

import java.awt.Component;
import java.awt.Frame;
import java.awt.GraphicsEnvironment;
import java.awt.Image;
import java.awt.Insets;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;

import com.tsarik.components.ColorSelector;




/**
 * Abstract font chooser dialog.
 * 
 * @author Phil Tsarik
 * @version 1.0.3
 *
 */
public abstract class AbstractFontChooser extends AbstractAplicationDialog {
	
	/**
	 * Constructs a font chooser.
	 * 
	 * @param owner The dialog's owner
	 * @param title Dialog title
	 * @param imageIcon Dialog icon
	 * @param w Dialog width
	 * @param h Dialog height
	 */
	public AbstractFontChooser(Frame owner, String title, Image imageIcon, int w, int h) {
		super(owner, title, imageIcon, w, h);
	}
	
	
	protected JComboBox comboboxFonts;
	protected JComboBox comboboxStyle;
	protected JSpinner spSize;
	protected ColorSelector csForegr;
	
	
	@Override
	protected Component createComponentPanel() {
		JPanel componentPanel = (JPanel)super.createComponentPanel();
		
		// init components
		comboboxFonts = new JComboBox();
		comboboxStyle = new JComboBox();
		spSize = new JSpinner(new SpinnerNumberModel(10, 1, 100, 1));
		csForegr = new ColorSelector();
		
		// fill Font list
		String fontList[] = GraphicsEnvironment.getLocalGraphicsEnvironment().getAvailableFontFamilyNames();
		for (int i = 0; i < fontList.length; i++)
			comboboxFonts.addItem(fontList[i]);
		
		// fill Style list
		comboboxStyle.addItem( "Plain" );
		comboboxStyle.addItem( "Bold" );
		comboboxStyle.addItem( "Italic" );
		comboboxStyle.addItem( "Bold Italic" );
		
		// create constraints
		constraints.insets = new Insets(5, 15, 10, 15);
		
		// place components onto the panel
		addComponentToGrid(new JLabel("Name:"), 0, 0, 1, 1, componentPanel);
		addComponentToGrid(new JLabel("Style:"), 0, 1, 1, 1, componentPanel);
		addComponentToGrid(new JLabel("Size:"), 0, 2, 1, 1, componentPanel);
		addComponentToGrid(new JLabel("Color:"), 0, 3, 1, 1, componentPanel);
		
		addComponentToGrid(comboboxFonts, 1, 0, 1, 1, componentPanel);
		addComponentToGrid(comboboxStyle, 1, 1, 1, 1, componentPanel);
		addComponentToGrid(spSize, 1, 2, 1, 1, componentPanel);
		addComponentToGrid(csForegr, 1, 3, 1, 1, componentPanel);
		return componentPanel;
	}
	
	
}
