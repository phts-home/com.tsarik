package com.tsarik.dialogs;

import java.awt.Component;
import java.awt.Container;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;

import javax.swing.JPanel;

import com.tsarik.dialogs.event.AbstractDialogEvent;
import com.tsarik.dialogs.event.AbstractDialogListener;


/**
 * Abstract application dialog with JPanel as Component Panel and GridBagLayout as layout in it.
 * 
 * @author Phil Tsarik
 * @version 0.7.0.20
 *
 */
public abstract class AbstractAplicationDialog extends AbstractDialog {

	public final int APPLY_CHECK_OK = 0;
	public final int APPLY_CHECK_FAIL = 1;
	
	/**
	 * Constructs a dialog with "OK","Cancel" and "Apply" buttons and binds "OK" and "Apply" buttons with apply() method
	 * 
	 * @param owner The dialog's owner as Frame object
	 * @param title Dialog title
	 * @param w Dialog width
	 * @param h Dialog height
	 */
	public AbstractAplicationDialog(Frame owner, String title, int w, int h) {
		super(owner, true, title, null, w, h);
		addAbstractDialogListener(new AbstractDialogListener(){
			@Override
			public void buttonPressed(AbstractDialogEvent evt) {
				if ( (evt.getButton() == AbstractDialogEvent.OK_BUTTON)|| (evt.getButton() == AbstractDialogEvent.APPLY_BUTTON) ) {
					apply();
				}
			}
		});
	}
	
	/**
	 * Constructs a dialog with "OK","Cancel" and "Apply" buttons and binds "OK" and "Apply" buttons with apply() method
	 * 
	 * @param owner The dialog's owner as Frame object
	 * @param title Dialog title
	 * @param imageIcon Dialog icon
	 * @param w Dialog width
	 * @param h Dialog height
	 */
	public AbstractAplicationDialog(Frame owner, String title, Image imageIcon, int w, int h) {
		super(owner, true, title, imageIcon, w, h);
		addAbstractDialogListener(new AbstractDialogListener(){
			@Override
			public void buttonPressed(AbstractDialogEvent evt) {
				if ( (evt.getButton() == AbstractDialogEvent.OK_BUTTON)|| (evt.getButton() == AbstractDialogEvent.APPLY_BUTTON) ) {
					apply();
				}
			}
		});
	}
	
	/**
	 * Constructs a dialog with custom buttons. Use AbstractDialogListener to handle button events. 
	 * 
	 * @param owner The dialog's owner as Frame object
	 * @param title Dialog title
	 * @param w Dialog width
	 * @param h Dialog height
	 * 
	 * @see com.tsarik.dialogs.event.AbstractDialogListener
	 * @see com.tsarik.dialogs.event.AbstractDialogEvent
	 */
	public AbstractAplicationDialog(Frame owner, String title, int w, int h, String[] buttonNames) {
		super(owner, true, title, null, w, h, buttonNames);
	}
	
	
	protected abstract int apply();
	protected GridBagConstraints constraints;
	
	
	@Override
	protected Component createComponentPanel() {
		JPanel panel = new JPanel();
		panel.setLayout(new GridBagLayout());
		constraints = new GridBagConstraints();
		constraints.fill = GridBagConstraints.HORIZONTAL;
		return panel;
	}
	
	/**
	 * Adds component to the layout grid.
	 * 
	 * @param c The component to add
	 * @param x X-coordinate in grid
	 * @param y Y-coordinate in grid
	 * @param w The number of cells in a row in the component's display area
	 * @param h The number of cells in a column in the component's display area
	 * @param target Container
	 */
	protected void addComponentToGrid(Component c, int x, int y, int w, int h, Container target) {
		constraints.gridx = x;
		constraints.gridy = y;
		constraints.gridwidth = w;
		constraints.gridheight = h;
		((GridBagLayout)target.getLayout()).setConstraints(c, constraints);
		target.add(c);
	}
	
	/**
	 * Adds component to the layout grid with some preinstall place, specified in level parameter.
	 * Various combinations of constraints' parameters(insets, weightx, weighty, fill, anchor)
	 * will be set depending on level
	 * 
	 * @param c The component to add
	 * @param x X-coordinate in grid
	 * @param y Y-coordinate in grid
	 * @param w The number of cells in a row in the component's display area
	 * @param h The number of cells in a column in the component's display area
	 * @param level A level parameter from 0 to 5 values
	 * @param target Container
	 */
	protected void addComponentToGrid(Component c, int x, int y, int w, int h, int level, Container target) {
		switch (level) {
		case 0: 
			if (constraints.insets == null) {
				constraints.insets = new Insets(1,1,2,1);
			} else {
				constraints.insets.set(1,1,2,1);
			}
			constraints.weightx = 1;
			constraints.weighty = 1;
			constraints.fill = GridBagConstraints.BOTH;
			constraints.anchor = GridBagConstraints.CENTER;
			break;
		case 1: 
			if (constraints.insets == null) {
				constraints.insets = new Insets(1,25,2,5);
			} else {
				constraints.insets.set(1,25,2,5);
			}
			constraints.weightx = 0;
			constraints.weighty = 0;
			constraints.fill = GridBagConstraints.HORIZONTAL;
			constraints.anchor = GridBagConstraints.NORTH;
			break;
		case 2:
			if (constraints.insets == null) {
				constraints.insets = new Insets(1,0,2,5);
			} else {
				constraints.insets.set(1,0,2,5);
			}
			constraints.weightx = 1;
			constraints.weighty = 0;
			constraints.fill = GridBagConstraints.HORIZONTAL;
			constraints.anchor = GridBagConstraints.CENTER;
			break;
		case 3:
			if (constraints.insets == null) {
				constraints.insets = new Insets(1,0,2,25);
			} else {
				constraints.insets.set(1,0,2,25);
			}
			constraints.weightx = 0;
			constraints.weighty = 0;
			constraints.fill = GridBagConstraints.HORIZONTAL;
			constraints.anchor = GridBagConstraints.CENTER;
			break;
		case 4:
			if (constraints.insets == null) {
				constraints.insets = new Insets(1,0,2,25);
			} else {
				constraints.insets.set(1,0,2,25);
			}
			constraints.weightx = 1;
			constraints.weighty = 0;
			constraints.fill = GridBagConstraints.HORIZONTAL;
			constraints.anchor = GridBagConstraints.CENTER;
			break;
		case 5:
			if (constraints.insets == null) {
				constraints.insets = new Insets(1,0,2,5);
			} else {
				constraints.insets.set(1,0,2,5);
			}
			constraints.weightx = 0;
			constraints.weighty = 0;
			constraints.fill = GridBagConstraints.NONE;
			constraints.anchor = GridBagConstraints.CENTER;
			break;
		}
		addComponentToGrid(c, x, y, w, h, target);
	}

}
