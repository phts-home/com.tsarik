package com.tsarik.components;

import java.awt.Component;
import java.awt.Graphics;
import java.awt.image.BufferedImage;

/**
 * Component that paints the specified image on itself.
 * 
 * @author Phil Tsarik
 * @version 0.8.1.20
 *
 */
public class ImageComponent extends Component {
	
	private static final long serialVersionUID = 30L;
	
	public ImageComponent() {
		image = null;
	}
	
	public ImageComponent(BufferedImage image) {
		this.image = image;
	}
	
	@Override
	public void paint(Graphics g) {
		super.paint(g);
		g.drawImage(image, 0, 0, null);
	}
	
	public BufferedImage getImage() {
		return image;
	}
	
	public void setImage(BufferedImage image) {
		this.image = image;
		if (image != null) {
			setSize(image.getWidth(), image.getHeight());
		}
		repaint();
	}
	
	protected BufferedImage image;
	
}
