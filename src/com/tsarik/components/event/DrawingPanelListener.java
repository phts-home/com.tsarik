package com.tsarik.components.event;

import java.util.*;

public interface DrawingPanelListener extends EventListener {
	
	public void stateChanged(DrawingPanelEvent evt);
	
}
