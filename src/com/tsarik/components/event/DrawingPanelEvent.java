package com.tsarik.components.event;

import java.awt.*;

public class DrawingPanelEvent {
	
	public DrawingPanelEvent(int tool, Color color, int penWidth) {
		this.tool = tool;
		this.color = color;
		this.penWidth = penWidth;
	}
	
	public int getTool() {
		return tool;
	}
	
	public Color getColor() {
		return color;
	}
	
	public int getPenWidth() {
		return penWidth;
	}
	
	private int tool;
	private Color color;
	private int penWidth;
	
}
