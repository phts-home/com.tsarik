package com.tsarik.components;

import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.BorderFactory;
import javax.swing.JColorChooser;
import javax.swing.JPanel;
import javax.swing.border.BevelBorder;


/**
 * Color selector which corresponds a panel colored in the selected color.
 * 
 * @author Phil Tsarik
 * @version 1.0.0
 *
 */
public class ColorSelector extends JPanel {
	
	private static final long serialVersionUID = 26L;
	
	/**
	 * Constructs a new instance of the <code>ColorSelector</code> class.
	 */
	public ColorSelector() {
		super();
		addMouseListener(new MouseEventsHandler());
		selectedColor = new Color(0, 0, 0);
		setBorder(BorderFactory.createBevelBorder(BevelBorder.RAISED));
		setBackground(selectedColor);
	}
	
	/**
	 * Gets the selected color.
	 * 
	 * @return The selected color
	 */
	public Color getSelectedColor() {
		return selectedColor;
	}
	
	/**
	 * Sets the specified color.
	 * 
	 * @param selectedColor The color to select
	 */
	public void setSelectedColor(Color selectedColor) {
		this.selectedColor = selectedColor;
		setBackground(selectedColor);
	}
	
	
	private Color selectedColor;
	
	
	/**
	 * Provides to receive mouse events.
	 * 
	 * @author Phil Tsarik
	 *
	 */
	private class MouseEventsHandler extends MouseAdapter {
		
		@Override
		public void mouseClicked(MouseEvent evt) {
			Color cl = JColorChooser.showDialog(ColorSelector.this, "Select Color", selectedColor);
			if (cl != null) {
				selectedColor = cl;
			}
			ColorSelector.this.setBackground(selectedColor);
		}
		
		@Override
		public void mousePressed(MouseEvent arg0) {
			ColorSelector.this.setBorder(BorderFactory.createBevelBorder(BevelBorder.LOWERED));
		}
		
		@Override
		public void mouseReleased(MouseEvent arg0) {
			ColorSelector.this.setBorder(BorderFactory.createBevelBorder(BevelBorder.RAISED));
		}
		
	}
	
}
