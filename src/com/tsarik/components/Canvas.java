package com.tsarik.components;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.image.BufferedImage;



/**
 * Component with two <code>BufferedImage</code>s as image containers. 
 * The second <code>BufferedImage</code> provides transparency.
 * 
 * @see com.tsarik.components.DrawingPanel
 * 
 * @author Phil Tsarik
 * @version 0.9.1.10
 *
 */
public class Canvas extends Component {
	
	private static final long serialVersionUID = 25L;
	
	public Canvas() {
		this(100, 100);
	}
	
	public Canvas(int w, int h) {
		setSize(w, h);
	}
	
	@Override
	public void paint(Graphics g) {
		g.drawImage(bi, 0, 0, null);
		g.drawImage(secondbi, 0, 0, null);
	}
	
	@Override
	public int getWidth() {
		return width;
	}
	
	@Override
	public int getHeight() {
		return height;
	}
	
	@Override
	public void setSize(int width, int height) {
		super.setSize(width, height);
		this.width = width;
		this.height = height;
		
		Color c;
		if (g == null) {
			c = Color.BLACK;
		} else {
			c = g.getColor();
		}
		
		bi = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
		secondbi = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
		
		g = bi.getGraphics();
		secondg = secondbi.getGraphics();
		
		g.setColor(c);
		secondg.setColor(c);
		
		clear();
	}
	
	public Graphics getImageGraphics() {
		return g;
	}
	
	public BufferedImage getImage() {
		return bi;
	}
	
	public Graphics getSecondImageGraphics() {
		return secondg;
	}
	
	public BufferedImage getSecondImage() {
		return secondbi;
	}
	
	public void clearSecondImage() {
		secondbi = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
		secondg = secondbi.getGraphics();
		
		Color c;
		if (g == null) {
			c = Color.BLACK;
		} else {
			c = g.getColor();
		}
		secondg.setColor(c);
	}
	
	public void setImage(BufferedImage image) {
		bi = image;
		if (image == null) {
			g = null;
			width = 0;
			height = 0;
			repaint();
			return;
		}
		g = image.getGraphics();
		width = image.getWidth();
		height = image.getHeight();
		
		clearSecondImage();
		
		super.setSize(width, height);
		repaint();
	}
	
	public void clear() {
		if (g == null) {
			return;
		}
		Color c = g.getColor();
		g.setColor(Color.WHITE);
		g.fillRect(0, 0, width, height);
		g.setColor(c);
		
		clearSecondImage();
		
		repaint();
	}
	
	protected int width;
	protected int height;
	
	protected BufferedImage bi;
	protected BufferedImage secondbi;
	protected Graphics g;
	protected Graphics secondg;
	
}

