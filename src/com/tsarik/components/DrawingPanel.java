package com.tsarik.components;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JPanel;
import javax.swing.event.EventListenerList;

import com.tsarik.components.event.*;

/**
 * Drawing panel. Includes tools (Pen, Rectangle, Ellipse, Eraser), pen width (thin, normal, thick), pen color.
 * 
 * @author Phil Tsarik
 * @version 0.8.2.50
 *
 */
public class DrawingPanel extends JPanel {
	
	private static final long serialVersionUID = 27L;
	
	public static final int TOOL_PEN = 1;
	public static final int TOOL_RECT = 2;
	public static final int TOOL_CIRCLE = 3;
	public static final int TOOL_ERASER = 4;
	public static final int TOOL_FILLEDRECT = 5;
	public static final int TOOL_FILLEDCIRCLE = 6;
	
	public static final Color COLOR_BACKGROUND = Color.WHITE;
	
	public DrawingPanel() {
		canvas = new Canvas();
		add(canvas);
		setCursor(Cursor.getPredefinedCursor(Cursor.CROSSHAIR_CURSOR));
		
		drawingPanelListenerList = new EventListenerList();
		
		setColor(Color.RED);
		setTool(TOOL_PEN);
		setPenWidth(2);
	}
	
	public DrawingPanel(int canvasWidth, int canvasHeight) {
		canvas = new Canvas(canvasWidth, canvasHeight);
		add(canvas);
		setCursor(Cursor.getPredefinedCursor(Cursor.CROSSHAIR_CURSOR));
		
		setColor(Color.RED);
		setTool(TOOL_PEN);
		setPenWidth(2);
	}
	
	public int getCanvasWidth() {
		return canvas.getWidth();
	}
	
	public int getCanvasHeight() {
		return canvas.getHeight();
	}
	
	public void setCanvasSize(int canvasWidth, int canvasHeight) {
		canvas.setSize(canvasWidth, canvasHeight);
	}
	
	public void clearImage() {
		canvas.clear();
	}
	
	public BufferedImage getImage() {
		return canvas.getImage();
	}
	
	public byte[] getImageData() throws IOException {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ImageIO.write(canvas.getImage(), "bmp", baos);
		return baos.toByteArray();
	}
	
	public void setImage(BufferedImage image) {
		canvas.setImage(image);
		setColor(color);
	}
	
	public void setImageData(byte[] data) throws IOException {
		BufferedImage image = ImageIO.read(new ByteArrayInputStream(data));
		canvas.setImage(image);
		setColor(color);
	}
	
	public Color getColor() {
		return color;
	}
	
	public void setColor(Color color) {
		this.color = color;
		if (tool != TOOL_ERASER) {
			canvas.getImageGraphics().setColor(color);
			canvas.getSecondImageGraphics().setColor(color);
		}
		fireChangeState(new DrawingPanelEvent(tool, color, penWidth));
	}
	
	public int getTool() {
		return tool;
	}
	
	public void setTool(int tool) {
		this.tool = tool;
		if (tool == TOOL_ERASER) {
			canvas.getImageGraphics().setColor(COLOR_BACKGROUND);
			canvas.getSecondImageGraphics().setColor(COLOR_BACKGROUND);
		} else {
			canvas.getImageGraphics().setColor(color);
			canvas.getSecondImageGraphics().setColor(color);
		}
		fireChangeState(new DrawingPanelEvent(tool, color, penWidth));
	}
	
	public int getPenWidth() {
		return penWidth;
	}
	
	public void setPenWidth(int penWidth) {
		this.penWidth = penWidth;
		fireChangeState(new DrawingPanelEvent(tool, color, penWidth));
	}
	
	/**
	 * Draws pen with the specified coordinates, color and pen width
	 * 
	 * @param coordinates Pen coordinates in format: coordinates[0] = x1, coordinates[1] = y1, coordinates[2] = x2, coordinates[3] = y2, ...
	 * @param red Red component of the color
	 * @param green Green component of the color
	 * @param blue Blue component of the color
	 * @param penWidth Pen width
	 */
	public void drawPen(int[] coordinates, int red, int green, int blue, int penWidth) {
		int x = coordinates[0];
		int y = coordinates[1];
		
		Graphics g = canvas.getImageGraphics();
		Color oldColor = g.getColor();
		g.setColor(new Color(red, green, blue));
		
		drawLine(x, y, x, y, penWidth);
		for (int i = 2; i < coordinates.length/2; i++) {
			drawLine(x, y, coordinates[2*i], coordinates[2*i+1], penWidth);
			x = coordinates[2*i];
			y = coordinates[2*i+1];
		}
		
		g.setColor(oldColor);
		canvas.repaint();
	}
	
	public void drawRect(int x1, int y1, int x2, int y2, int red, int green, int blue, int penWidth, boolean filled) {
		Graphics g = canvas.getImageGraphics();
		Color oldColor = g.getColor();
		g.setColor(new Color(red, green, blue));
		if (filled) {
			fillRect(x1, y1, x2, y2);
		} else {
			drawRect(x1, y1, x2, y2, penWidth);
		}
		g.setColor(oldColor);
		canvas.repaint();
	}
	
	public void drawCircle(int x1, int y1, int x2, int y2, int red, int green, int blue, int penWidth, boolean filled) {
		Graphics g = canvas.getImageGraphics();
		Color oldColor = g.getColor();
		g.setColor(new Color(red, green, blue));
		if (filled) {
			fillCircle(x1, y1, x2, y2);
		} else {
			drawCircle(x1, y1, x2, y2, penWidth);
		}
		g.setColor(oldColor);
		canvas.repaint();
	}
	
	public void drawEraser(int[] coordinates, int penWidth) {
		int x = coordinates[0];
		int y = coordinates[1];
		
		Graphics g = canvas.getImageGraphics();
		Color oldColor = g.getColor();
		g.setColor(COLOR_BACKGROUND);

		drawLine(x, y, x, y, penWidth);
		for (int i = 2; i < coordinates.length/2; i++) {
			drawLine(x, y, coordinates[2*i], coordinates[2*i+1], penWidth);
			x = coordinates[2*i];
			y = coordinates[2*i+1];
		}
		
		g.setColor(oldColor);
		canvas.repaint();
	}
	
	
	
	public void addDrawingPanelListener(DrawingPanelListener l) {
		drawingPanelListenerList.add(DrawingPanelListener.class, l);
	}
	
	public void removeDrawingPanelListener(DrawingPanelListener l) {
		drawingPanelListenerList.remove(DrawingPanelListener.class, l);
	}
	
	public DrawingPanelListener[] getDrawingPanelListeners() {
		return drawingPanelListenerList.getListeners(DrawingPanelListener.class);
	}
	
	
	
	protected Canvas canvas;
	protected Color color;
	protected int tool;
	protected int penWidth;
	protected EventListenerList drawingPanelListenerList;
	
	
	protected void fireChangeState(DrawingPanelEvent evt) {
		Object[] listeners = drawingPanelListenerList.getListenerList();
		for (Object i : listeners) {
			if (i instanceof DrawingPanelListener) {
				((DrawingPanelListener)i).stateChanged(evt);
			}
		}
	}
	
	protected void drawLine(int x1, int y1, int x2, int y2, int penWidth) {
		Graphics g = canvas.getImageGraphics();
		switch (penWidth) {
		case 2:
			g.drawLine(x1, y1, x2, y2);
			g.drawLine(x1+1, y1, x2+1, y2);
			g.drawLine(x1-1, y1, x2-1, y2);
			g.drawLine(x1, y1+1, x2, y2+1);
			g.drawLine(x1, y1-1, x2, y2-1);
			break;
		case 3:
			g.drawLine(x1, y1, x2, y2);
			g.drawLine(x1+1, y1, x2+1, y2);
			g.drawLine(x1+2, y1, x2+2, y2);
			g.drawLine(x1-1, y1, x2-1, y2);
			g.drawLine(x1-2, y1, x2-2, y2);
			g.drawLine(x1, y1+1, x2, y2+1);
			g.drawLine(x1, y1+2, x2, y2+2);
			g.drawLine(x1, y1-1, x2, y2-1);
			g.drawLine(x1, y1-2, x2, y2-2);
			break;
		default:
			g.drawLine(x1, y1, x2, y2);
		}
	}
	
	protected void drawRect(int x1, int y1, int x2, int y2, int penWidth) {
		drawRect(x1, y1, x2, y2, penWidth, canvas.getImageGraphics());
	}
	
	protected void drawRect(int x1, int y1, int x2, int y2, int penWidth, Graphics g) {
		switch (penWidth) {
		case 2:
			if (x1 <= x2) {
				if (y1 <= y2) {
					g.drawRect(x1, y1, x2-x1, y2-y1);
					g.drawRect(x1+1, y1+1, x2-x1-2, y2-y1-2);
				} else {
					g.drawRect(x1, y2, x2-x1, y1-y2);
					g.drawRect(x1+1, y2+1, x2-x1-2, y1-y2-2);
				}
			} else {
				if (y1 <= y2) {
					g.drawRect(x2, y1, x1-x2, y2-y1);
					g.drawRect(x2+1, y1+1, x1-x2-2, y2-y1-2);
				} else {
					g.drawRect(x2, y2, x1-x2, y1-y2);
					g.drawRect(x2+1, y2+1, x1-x2-2, y1-y2-2);
				}
			}
			break;
		case 3:
			if (x1 <= x2) {
				if (y1 <= y2) {
					g.drawRect(x1, y1, x2-x1, y2-y1);
					g.drawRect(x1+1, y1+1, x2-x1-2, y2-y1-2);
					g.drawRect(x1-1, y1-1, x2-x1+2, y2-y1+2);
				} else {
					g.drawRect(x1, y2, x2-x1, y1-y2);
					g.drawRect(x1+1, y2+1, x2-x1-2, y1-y2-2);
					g.drawRect(x1-1, y2-1, x2-x1+2, y1-y2+2);
				}
			} else {
				if (y1 <= y2) {
					g.drawRect(x2, y1, x1-x2, y2-y1);
					g.drawRect(x2+1, y1+1, x1-x2-2, y2-y1-2);
					g.drawRect(x2-1, y1-1, x1-x2+2, y2-y1+2);
				} else {
					g.drawRect(x2, y2, x1-x2, y1-y2);
					g.drawRect(x2+1, y2+1, x1-x2-2, y1-y2-2);
					g.drawRect(x2-1, y2-1, x1-x2+2, y1-y2+2);
				}
			}
			break;
		default:
			if (x1 <= x2) {
				if (y1 <= y2) {
					g.drawRect(x1, y1, x2-x1, y2-y1);
				} else {
					g.drawRect(x1, y2, x2-x1, y1-y2);
				}
			} else {
				if (y1 <= y2) {
					g.drawRect(x2, y1, x1-x2, y2-y1);
				} else {
					g.drawRect(x2, y2, x1-x2, y1-y2);
				}
			}
		}
	}
	
	protected void fillRect(int x1, int y1, int x2, int y2) {
		fillRect(x1, y1, x2, y2, canvas.getImageGraphics());
	}
	
	protected void fillRect(int x1, int y1, int x2, int y2, Graphics g) {
		if (x1 <= x2) {
			if (y1 <= y2) {
				g.fillRect(x1, y1, x2-x1, y2-y1);
			} else {
				g.fillRect(x1, y2, x2-x1, y1-y2);
			}
		} else {
			if (y1 <= y2) {
				g.fillRect(x2, y1, x1-x2, y2-y1);
			} else {
				g.fillRect(x2, y2, x1-x2, y1-y2);
			}
		}
	}
	
	protected void drawCircle(int x1, int y1, int x2, int y2, int penWidth) {
		drawCircle(x1, y1, x2, y2, penWidth, canvas.getImageGraphics());
	}
	
	protected void drawCircle(int x1, int y1, int x2, int y2, int penWidth, Graphics g) {
		switch (penWidth) {
		case 2:
			if (x1 <= x2) {
				if (y1 <= y2) {
					g.drawOval(x1, y1, x2-x1, y2-y1);
					g.drawOval(x1+1, y1+1, x2-x1-2, y2-y1-2);
				} else {
					g.drawOval(x1, y2, x2-x1, y1-y2);
					g.drawOval(x1+1, y2+1, x2-x1-2, y1-y2-2);
				}
			} else {
				if (y1 <= y2) {
					g.drawOval(x2, y1, x1-x2, y2-y1);
					g.drawOval(x2+1, y1+1, x1-x2-2, y2-y1-2);
				} else {
					g.drawOval(x2, y2, x1-x2, y1-y2);
					g.drawOval(x2+1, y2+1, x1-x2-2, y1-y2-2);
				}
			}
			break;
		case 3:
			if (x1 <= x2) {
				if (y1 <= y2) {
					g.drawOval(x1, y1, x2-x1, y2-y1);
					g.drawOval(x1+1, y1+1, x2-x1-2, y2-y1-2);
					g.drawOval(x1-1, y1-1, x2-x1+2, y2-y1+2);
				} else {
					g.drawOval(x1, y2, x2-x1, y1-y2);
					g.drawOval(x1+1, y2+1, x2-x1-2, y1-y2-2);
					g.drawOval(x1-1, y2-1, x2-x1+2, y1-y2+2);
				}
			} else {
				if (y1 <= y2) {
					g.drawOval(x2, y1, x1-x2, y2-y1);
					g.drawOval(x2+1, y1+1, x1-x2-2, y2-y1-2);
					g.drawOval(x2-1, y1-1, x1-x2+2, y2-y1+2);
				} else {
					g.drawOval(x2, y2, x1-x2, y1-y2);
					g.drawOval(x2+1, y2+1, x1-x2-2, y1-y2-2);
					g.drawOval(x2-1, y2-1, x1-x2+2, y1-y2+2);
				}
			}
			break;
		default:
			if (x1 <= x2) {
				if (y1 <= y2) {
					g.drawOval(x1, y1, x2-x1, y2-y1);
				} else {
					g.drawOval(x1, y2, x2-x1, y1-y2);
				}
			} else {
				if (y1 <= y2) {
					g.drawOval(x2, y1, x1-x2, y2-y1);
				} else {
					g.drawOval(x2, y2, x1-x2, y1-y2);
				}
			}
		}
	}
	
	protected void fillCircle(int x1, int y1, int x2, int y2) {
		fillCircle(x1, y1, x2, y2, canvas.getImageGraphics());
	}
	
	protected void fillCircle(int x1, int y1, int x2, int y2, Graphics g) {
		if (x1 <= x2) {
			if (y1 <= y2) {
				g.fillOval(x1, y1, x2-x1, y2-y1);
			} else {
				g.fillOval(x1, y2, x2-x1, y1-y2);
			}
		} else {
			if (y1 <= y2) {
				g.fillOval(x2, y1, x1-x2, y2-y1);
			} else {
				g.fillOval(x2, y2, x1-x2, y1-y2);
			}
		}
	}
	
}
