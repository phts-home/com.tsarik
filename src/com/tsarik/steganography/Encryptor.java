package com.tsarik.steganography;

import java.io.File;
import java.io.IOException;

public interface Encryptor {
	
	public void encrypt(File in, File out, String text) throws UnableToEncodeException, IOException;
	
	public String decrypt(File in) throws UnableToDecodeException, IOException;
	
}
